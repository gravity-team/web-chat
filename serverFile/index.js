// Connect to server1
let io = require('socket.io-client');
let domainChatServer = process.env.NODE_ENV !== 'production' ? 'http://localhost' : 'https://react-chat-app-demo.herokuapp.com';
let socket = io.connect(domainChatServer, { reconnect: true, origins: domainChatServer });

//const constants = require('../server/app/config/constants');
//const serverSendFile = constants.serverSendFile;
//import { serverSendFile } from '../server/app/config/constants';
let DicUserSendFile = {};
let DicMaskSocket = {};
console.log('server SendFile running.....');
// Add a connect listener
socket.on('connect', function (socket) {
    console.log('Connected!');
});

//gui tin hieu server1 luu socketID cuar server2
console.log('send socketID');
socket.emit('sendSocketID', 'nope');

socket.on('NotifySendFile', (data) => {
    console.log(data);

});

////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
//Create serverSendFile
const port = process.env.PORT || 2020;
console.log('running on port: ' + port);
const dl = require('./lib/delivery.server');
const fs = require('fs');

const express = require('express')
const cors = require('cors')
const app = express()


const corsOptions = {
    origin: domainChatServer,
    optionsSuccessStatus: 200
}

app.get('*', cors(corsOptions), function (req, res, next) {
    res.sendFile('./index.html');
})

const server = require('http').createServer(app).listen(port)

const io2 = require('socket.io').listen(server);


io2.sockets.on('connection', (socket) => {
    console.log('New client connect!');
    delivery = dl.listen(socket);

    /**
     * data.mask
     */
    socket.on('sendSocketID', (data) => {
        console.log('Socket: ' + data.mask);
        DicMaskSocket[data.mask] = socket;
    });

    /**
     * file
     * data.sender
     * data.receiver
     */
    SendFile = (fileName, fileBuffer) => {
        return new Promise((resolve, reject) => {
            fs.writeFile(fileName, fileBuffer, function (err) {
                if (err) {
                    console.log('fileName: ');
                    console.log(fileName);
                    console.log('fileBuffer: ');
                    console.log(fileBuffer);
                    resolve(false);
                } else {
                    resolve(true);
                };
            });
        });
    }

    socket.on('sendFile', (data) => {
        // WriteFile(file.name, file.buffer)
        //     .then((result) => {
        //         console.log('**SendFile**');
        //         if (result == true) {
        //             //gui tin hieu client B nhan file
        //             console.log('NotifyReceiveFile');
        //             DicMaskSocket[data.receive].emit('NotifyReceiveFile', 'ju');
        //             //
        //             delivery.on('delivery.sendFile', (delivery) => {
        //                 //test
        //                 delivery.send({
        //                     name: 'image.jpg',
        //                     path: './image.jpg'
        //                 });
        //             });
        //         }
        //     })
        //     .catch((error) => {
        //         console.log(error);
        //     });
        console.log('receiver: ' + data.mask);
        if (DicMaskSocket[data.mask]) {
            console.log('sent');
            DicMaskSocket[data.mask].emit('NotifyReceiveFile', { file: data.file, fileName: data.fileName, sender: data.sender });
        }
    })
});
