import { createStore, applyMiddleware, compose } from 'redux';
import RootReducer from '../reducer/RootReducer';

export default function configureStore(initialState) {
  const store = createStore(
    RootReducer,
    compose(
      applyMiddleware()
    )
  )

  return store;
}