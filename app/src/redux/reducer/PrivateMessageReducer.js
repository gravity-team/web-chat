import { privateMessageAction } from '../constants/ActionConstants';

const privateMessageReducer = (state = { privateMessages: {} }, action) => {
  switch (action.type) {
    case privateMessageAction.ADD_MESSAGE:
      return {
        ...state, ...{
          privateMessages: {
            ...state.privateMessages,
            [action.message.key]: {
              ...state.privateMessages[action.message.key], ...{
                messages: [
                  ...state.privateMessages[action.message.key].messages || [],
                  action.message.body
                ],
                sendMessage: action.message.sendMessage
              }
            }
          }
        }
      };
    case privateMessageAction.ADD_TAB:
      return { ...state, ...{ privateMessages: { ...state.privateMessages, ...action.tab } } };
    case privateMessageAction.TOGGLE:
      return {
        ...state, ...{
          privateMessages: {
            ...state.privateMessages,
            [action.tab.key]: { ...state.privateMessages[action.tab.key], ...action.tab.toggle }
          }
        }
      };
    case privateMessageAction.RESET:
      return { privateMessages: {} }
    default:
      return state;
  }
}

export default privateMessageReducer;