import { combineReducers } from 'redux';
import MessageReducer from './MessageReducer';
import EmojiReducer from './EmojiReducer';
import OnlineUserReducer from './OnlineUserReducer';
import PrivateMessageReducer from './PrivateMessageReducer';

const rootReducer = combineReducers({
  MessageReducer,
  EmojiReducer,
  OnlineUserReducer,
  PrivateMessageReducer
})

export default rootReducer;