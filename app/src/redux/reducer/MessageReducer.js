import { messageAction } from '../constants/ActionConstants';

const messageReducer = (state = { messages: [], sendMessage: true }, action) => {
  switch (action.type) {
    case messageAction.SEND_MESSAGE:
      return { ...state, messages: [...state.messages, action.message.body], sendMessage: action.message.sendMessage };
    case messageAction.RECEIVE_MESSAGE:
      return state;
    case messageAction.RESET:
      return { messages: [], sendMessage: true }
    default:
      return state;
  }
}

export default messageReducer;