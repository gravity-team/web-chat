import { authenticationAction } from '../constants/ActionConstants';

const authenticationReducer = (state = { token: '', username: '' }, action) => {
  switch (action.type) {
    case authenticationAction.INIT_INFO:
      return { ...state, token: action.token };
    case authenticationAction.LOGIN:
      return state;
    default:
      return state;
  }
}

export default authenticationReducer;