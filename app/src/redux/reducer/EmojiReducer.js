import { emojiAction } from '../constants/ActionConstants';

const emojiReducer = (state = { emoji: '' }, action) => {
  switch (action.type) {
    case emojiAction.USE_EMOJI:
      return { emoji: action.emoji };
    case emojiAction.RESET_EMOJI:
      return { emoji: '' }
    default:
      return state;
  }
}

export default emojiReducer;