import { onlineUserAction } from '../constants/ActionConstants';

const onlineUserReducer = (state = { users: [] }, action) => {
  switch (action.type) {
    case onlineUserAction.INIT_USERS:
      return { ...state, users: action.users };
    case onlineUserAction.ADD_USER:
      return { ...state, users: [...state.users, action.user] }
    case onlineUserAction.REMOVE_USER:
      return { ...state, users: [...state.users, state.users.splice(state.users.indexOf(action.user), 1)] }
    default:
      return state;
  }
}

export default onlineUserReducer;