// Const action for message
export const messageAction = {
  SEND_MESSAGE: 'SEND_MESSAGE',
  RECEIVE_MESSAGE: 'RECEIVE_MESSAGE',
  RESET: 'RESET'
}

// Const action for emoji
export const emojiAction = {
  USE_EMOJI: 'USE_EMOJI',
  RESET_EMOJI: 'RESET_EMOJI'
}

// Const action for authentication
export const authenticationAction = {
  INIT_INFO: 'INIT_INFO',
  SIGN_UP: 'SIGN_UP',
  LOGIN: 'LOGIN'
}

// Const action for online user
export const onlineUserAction = {
  INIT_USERS: 'INIT_USERS',
  ADD_USER: 'ADD_USER',
  REMOVE_USER: 'REMOVE_USER'
}

export const privateMessageAction = {
  ADD_MESSAGE: 'ADD_MESSAGE',
  ADD_TAB: 'ADD_TAB',
  TOGGLE: 'TOGGLE',
  RESET: 'RESET'
}