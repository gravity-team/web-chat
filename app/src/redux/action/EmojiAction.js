import { emojiAction } from '../constants/ActionConstants';

export const useEmoji = (emoji) => {
  return { type: emojiAction.USE_EMOJI, emoji };
}

export const resetEmoji = () => {
  return { type: emojiAction.RESET_EMOJI };
}