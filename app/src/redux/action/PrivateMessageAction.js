import { privateMessageAction } from '../constants/ActionConstants';

export const addMessage = (message) => {
  return { type: privateMessageAction.ADD_MESSAGE, message };
}

export const addTab = (tab) => {
  return { type: privateMessageAction.ADD_TAB, tab };
}

export const toggle = (tab) => {
  return { type: privateMessageAction.TOGGLE, tab };
}

export const resetPrivate = () => {
  return { type: privateMessageAction.RESET }
}