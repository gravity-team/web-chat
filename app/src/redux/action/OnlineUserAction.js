import { onlineUserAction } from '../constants/ActionConstants';

export const initUsers = (users) => {
  return { type: onlineUserAction.INIT_USERS, users };
}

export const addUser = (user) => {
  return { type: onlineUserAction.ADD_USER, user };
}

export const removeUser = (user) => {
  return { type: onlineUserAction.REMOVE_USER, user };
}