import { messageAction } from '../constants/ActionConstants';

export const sendMessage = (message) => {
  return { type: messageAction.SEND_MESSAGE, message };
}

export const receiveMessage = () => {
  return { type: messageAction.RECEIVE_MESSAGE };
}

export const reset = () => {
  return { type: messageAction.RESET };
}