import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { browserHistory } from 'react-router';
import { Cookies } from 'react-cookie';
import App from './components/App';
import ChatRoom from './components/ChatRoom';
import { socketConfig } from './redux/constants/SocketConsstants';

const cookies = new Cookies();
const requireAuth = (nextState, replace) => {
  let authToken = cookies.get('authToken');
  if (!authToken) {
    replace({
      pathname: '/',
    });
  } else {
    let socket = nextState.routes[1].socket;
    socket.emit(socketConfig.VERIFY_AUTH_TOKEN, { authToken: authToken });

    socket.on(socketConfig.VERIFY_AUTH_TOKEN_RESULT, (data) => {
      if (data.success) {
        browserHistory.replace('/chat-room');
      } else {
        browserHistory.replace('/');
      }
    });
  }
};

const redirectIfHasToken = (nextState, replace) => {
  let authToken = cookies.get('authToken');
  if (authToken) {
    let socket = nextState.routes[1].socket;
    socket.emit(socketConfig.VERIFY_AUTH_TOKEN, { authToken: authToken });

    socket.on(socketConfig.VERIFY_AUTH_TOKEN_RESULT, (data) => {
      if (data.success) {
        browserHistory.replace('/chat-room');
      }
    });
  }
}

const routes = (socket) => (
  <Switch>
    <Route path='/' component={App} socket={socket} cookies={cookies} onEnter={redirectIfHasToken} />
    <Route path='/chat-room' component={ChatRoom} socket={socket} cookies={cookies} onEnter={requireAuth} />
  </Switch>
);

export default routes;