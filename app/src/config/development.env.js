const config = {
  ENV: 'staging',
  SERVER_DOMAIN: 'localhost:8080'
}

export default config;