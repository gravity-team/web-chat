const config = {
  ENV: 'production',
  SERVER_DOMAIN: 'https://react-chat-app-demo.herokuapp.com'
}

export default config;