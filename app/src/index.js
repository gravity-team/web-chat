import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Router } from 'react-router';
import { CookiesProvider } from 'react-cookie';
import { browserHistory } from 'react-router';
import configureStore from './redux/store/configureStore';
import routes from './routes';
import './assets/js/StringExtension';
import './assets/css/main.scss';
import io from 'socket.io-client';
import config from './config/env';

const initialState = window.__INITIAL_STATE__;
const store = configureStore(initialState);

let socket = io(config.SERVER_DOMAIN);

ReactDOM.render(
  (<Provider store={store}>
    <CookiesProvider>
      <Router history={browserHistory} routes={routes(socket)} socket={socket} />
    </CookiesProvider>
  </Provider>), document.getElementById('root')
);