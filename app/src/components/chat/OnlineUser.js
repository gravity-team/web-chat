import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Avatar } from 'antd';
import { socketConfig } from '../../redux/constants/SocketConsstants';
import { initUsers, addUser, removeUser } from '../../redux/action/OnlineUserAction';
import { addTab, toggle } from '../../redux/action/PrivateMessageAction';

class OnlineUser extends Component {
  constructor(props) {
    super(props);
    this.state = {
      users: []
    }
    this.socket = this.props.socket;
    this.initSocket(this.socket);

    this.props.socket.emit(socketConfig.REQUEST_LIST_ONLINE_USER);
  }

  initSocket = (socket, cookies) => {
    socket.on(socketConfig.NEW_ONLINE_USER, (data) => {
      this.props.addUser(data);
    });

    socket.on(socketConfig.RECEIVE_LIST_ONLINE_USER, (data) => {
      this.props.initUsers(data);
    });
  }

  componentDidMount() {
    if (!this.props.users.length) {
      this.socket.emit(socketConfig.REQUEST_LIST_ONLINE_USER);
    }
  }

  newPrivateMessage = (event) => {
    let node = document.getElementById(event.target.parentElement.dataset.mask);
    if (!node) {
      this.props.addTab({
        [event.target.parentElement.dataset.mask]: {
          mask: event.target.parentElement.dataset.mask,
          fullName: event.target.parentElement.dataset.name,
          toggle: true
        }
      });
    } else {
      this.props.toggle({ key: event.target.parentElement.dataset.mask, toggle: { toggle: true } });
    }
  }

  render() {
    const users = this.props.users.map((user, index) => {
      return (
        <div key={index} data-mask={user.mask} data-name={user.fullName} className='user-block flex' onClick={this.newPrivateMessage}>
          <Avatar icon='user' style={{ backgroundColor: '#87d068' }} />
          <div className='user-name'>{user.fullName}</div>
        </div>
      );
    });

    return (
      <section className='online-user-list'>
        {users}
      </section>
    );
  }
}

const mapStateToProps = state => ({ users: state.OnlineUserReducer.users });

const mapDispatchToProps = {
  initUsers,
  addUser,
  removeUser,
  addTab,
  toggle
}

export default connect(mapStateToProps, mapDispatchToProps)(OnlineUser);