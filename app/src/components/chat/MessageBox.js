import React, { Component } from 'react';
import { connect } from 'react-redux';
import { sendMessage } from '../../redux/action/MessageAction';
import Emoji from './Emoji';
import { useEmoji, resetEmoji } from '../../redux/action/EmojiAction';
import { Icon, Popover } from 'antd';
import 'antd/lib/icon/style/css';
import 'antd/lib/popover/style/css';
import { socketConfig } from '../../redux/constants/SocketConsstants';

class MessageBox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      message: ''
    }
    this.cookies = this.props.cookies;
    this.initSocket(this.props.socket);
  }

  initSocket = (socket) => {
    const that = this;
    socket.on('receive', (data) => {
      that.props.sendMessage({ body: { content: data.message, sender: data.sender, auth_token: '', receiver: '', from: 'other', type: 'text' }, sendMessage: false });
    });
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ message: this.state.message + nextProps.emoji });
    this.props.resetEmoji('');
  }

  changeText = (e) => this.setState({ message: e.target.value });

  pressEnter = (e) => {
    if ((e.ctrlKey || e.metaKey) && (e.keyCode === 13 || e.keyCode === 10)) {
      this.setState({
        message: e.target.value + '\n'
      });
      e.preventDefault();
    } else if (e.keyCode === 13) {
      if (this.state.message.trim().length > 0) {
        this.sendMessage();
      }
      e.preventDefault();
    }
  }

  sendMessage = () => {
    this.props.sendMessage({ body: { content: this.state.message, sender: this.cookies.get('name'), auth_token: this.cookies.get('authToken'), receiver: '', from: 'self', type: 'text' }, sendMessage: true });

    this.props.socket.emit('message', { message: this.state.message, sender: this.cookies.get('name') });

    this.setState({
      message: ''
    });
  }

  sendFile = () => {
    this.props.socket.emit(socketConfig.REQUEST_SEND_FILE);
  }

  render() {
    return (
      <section className='message-box flex'>
        <textarea onChange={this.changeText} value={this.state.message} onKeyDown={this.pressEnter.bind(this)}></textarea>
        <div className='button-group flex'>
          <Popover placement='topRight' content={<Emoji />} trigger='click'>
            <Icon type='smile-o' />
          </Popover>
          <Icon type='cloud'><input type="file" name="image-uploader" className="upload-file" /></Icon>
          <Icon type='camera'><input type="file" name="image-uploader" accept="image/*" className="upload-file" /></Icon>
        </div>
      </section>
    );
  }
}

const mapStateToProps = state => ({
  emoji: state.EmojiReducer.emoji
});

const mapDispatchToProps = {
  sendMessage,
  useEmoji,
  resetEmoji
}

export default connect(mapStateToProps, mapDispatchToProps)(MessageBox);