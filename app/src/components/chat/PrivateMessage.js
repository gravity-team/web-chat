import React, { Component } from 'react';
import { connect } from 'react-redux';
import { socketConfig } from '../../redux/constants/SocketConsstants';
import { Icon } from 'antd';
import { toggle, addMessage } from '../../redux/action/PrivateMessageAction';
import io from 'socket.io-client';

class PrivateMessage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      message: '',
      userName: this.props.cookies.get('name'),
      cloud: false,
      photo: false
    }
    this.cookies = this.props.cookies;
    this.initSocket(this.props.socket);
  }

  componentDidMount() {
    this.scrollToBottom();
  }

  componentDidUpdate() {
    this.scrollToBottom();
  }

  initSocket = (socket) => {
    let that = this;
    socket.on(socketConfig.RECEIVE_PORT_SEND_FILE, (data) => {
      let s = io(data.addressSendFile);
      if (that.state.sendFile) {
        that.setState({ sendFile: false });

        that.sendSocketId(s, that).then(() => {
          s.emit('sendFile', {
            mask: that.props.user.mask,
            file: that.state.file,
            fileName: that.state.fileName,
            sender: that.cookies.get('name')
          });
          this.props.addMessage({ key: this.props.user.mask, body: { content: that.state.fileName, sender: that.cookies.get('name'), time: that.state.timeStamp, from: 'self', type: 'file' }, sendMessage: true });
        });
      } else {
        that.sendSocketId(s, that);
      }

      s.on('NotifyReceiveFile', (data) => {
        let timeStamp = Date.now();
        let file = decodeURIComponent(data.file);
        that.setState({ files: { ...that.state.files, [timeStamp]: file } });
        this.props.addMessage({ key: this.props.user.mask, body: { content: data.fileName, sender: data.sender, time: timeStamp, from: 'other', type: 'file' }, sendMessage: true });
        s.close();
      });
    });
  }

  sendSocketId = (s, that) => {
    return new Promise((resolve, reject) => {
      s.emit('sendSocketID', {
        mask: that.props.cookies.get('mask')
      });
      resolve(true);
    });
  }

  toggle = () => {
    this.props.toggle({ key: this.props.user.mask, toggle: { toggle: !this.props.user.toggle } });
  }

  changeText = (e) => this.setState({ message: e.target.value });

  pressEnter = (e) => {
    if ((e.ctrlKey || e.metaKey) && (e.keyCode === 13 || e.keyCode === 10)) {
      this.setState({
        message: e.target.value + '\n'
      });
      e.preventDefault();
    } else if (e.keyCode === 13) {
      if (this.state.message.trim().length > 0) {
        this.sendMessage();
      }
      e.preventDefault();
    }
  }

  sendMessage = () => {
    this.props.addMessage({ key: this.props.user.mask, body: { content: this.state.message, sender: this.state.userName, auth_token: this.cookies.get('authToken'), from: 'self', type: 'text' }, sendMessage: true });

    this.props.socket.emit(socketConfig.PRIVATE_MESSAGE, {
      message: this.state.message,
      senderName: this.cookies.get('name'),
      mask: this.props.user.mask
    });

    this.setState({
      message: ''
    });
  }

  scrollToBottom = () => {
    if (this.props.privateMessages[this.props.user.mask] && this.props.privateMessages[this.props.user.mask].sendMessage && this.endMessage != null) {
      this.endMessage.scrollIntoView({ behavior: "smooth" });
    }
  }

  sendFile = (event) => {
    let that = this;
    that.props.socket.emit('NOTIFY_POPUP_CHAT', {
      sender: that.cookies.get('mask'),
      senderName: that.cookies.get('name'),
      mask: that.props.user.mask,
      receiver: that.props.user.mask
    })
    this.setState({ cloud: true, photo: true, sendFile: true, fileName: event.target.files[0].name });
    let fr = new FileReader();
    fr.onload = function () {
      let f = this.result;
      new Promise((resolve, reject) => {
        that.props.socket.emit(socketConfig.REQUEST_SEND_FILE, {
          mask: that.props.user.mask,
          senderName: that.cookies.get('name')
        });
        resolve(true);
      }).then(() => {
        let timeStamp = Date.now();
        that.setState({ cloud: false, photo: false, file: encodeURIComponent(f), files: { ...that.state.files, [timeStamp]: f }, timeStamp: timeStamp });
      });
    }
    fr.readAsDataURL(event.target.files[0]);
  }

  render() {
    if (!this.props.hidden) {
      let messages = this.props.privateMessages[this.props.user.mask] || {};
      messages = messages.messages || [];
      messages = messages.map((message, index) => {
        let blockClassName = `message-block ${message.from}`;
        let messageClassName = `message ${message.from}`;
        let senderName = '';
        if (this.state.userName !== message.sender) {
          senderName = <span className="sender-name">{message.sender}:</span>
        }
        if (message.type === 'file') {
          let file = this.state.files[message.time];
          if (file.substr(0, file.indexOf('/')).includes('image')) {
            return (
              <div key={index} className={blockClassName}>
                <div className={messageClassName}>
                  {senderName}
                  <img src={file} />
                </div>
              </div>
            );
          } else {
            return (
              <div key={index} className={blockClassName}>
                <div className={messageClassName}>
                  {senderName}
                  <a href={file} download={message.content}>{message.content}</a>
                </div>
              </div>
            );
          }
        } else {
          return (
            <div key={index} className={blockClassName}>
              <div className={messageClassName}>
                {senderName}
                {
                  message.content.split('\n').map((line, i) => {
                    return <span key={i}>{line}<br /></span>
                  })
                }
              </div>
            </div>
          );
        }
      });

      return (
        <section className='private-message flex flex-column'>
          <section onClick={this.toggle} id={this.props.user.mask} className="name-block">{this.props.user.fullName}</section>
          <section className='message-list'>
            {messages}
            <div ref={node => this.endMessage = node}></div>
          </section>
          <section className='message-box flex flex-column'>
            <textarea onChange={this.changeText} value={this.state.message} onKeyDown={this.pressEnter.bind(this)}></textarea>
            <div className='button-group flex'>
              <Icon type='cloud'><input type="file" name="file-uploader" className="upload-file" onChange={this.sendFile.bind(this)} disabled={this.state.cloud} /></Icon>
              <Icon type='camera'><input type="file" name="image-uploader" accept="image/*" className="upload-file" onChange={this.sendFile.bind(this)} disabled={this.state.photo} /></Icon>
            </div>
          </section>
        </section>
      );
    } else {
      return null;
    }
  }
}

const mapStateToProps = state => ({ privateMessages: state.PrivateMessageReducer.privateMessages });

const mapDispatchToProps = {
  toggle,
  addMessage
}

export default connect(mapStateToProps, mapDispatchToProps)(PrivateMessage);