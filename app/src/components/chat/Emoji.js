import React, { Component } from 'react';
import { connect } from 'react-redux';
import { emoji } from '../../assets/js/emoji';
import { useEmoji } from '../../redux/action/EmojiAction';
import { Tabs } from 'antd';
import 'antd/lib/tabs/style/css';

const TabPane = Tabs.TabPane;

class Emoji extends Component {
  constructor(props) {
    super(props);
    this.state = {
      emoji: emoji
    }
  }

  useEmojiIcon = (e) => {
    this.props.useEmoji(e.target.textContent);
  }

  render() {
    const { emoji = {} } = this.state;

    const tabPane = Object.entries(emoji).map(([key, value], index) => (
      <TabPane tab={key.humanize()} key={index}>
        <div className='flex flex-wrap scroll-block'>
          {value.map((emo, i) => <label key={i} onClick={this.useEmojiIcon}>{emo}</label>)}
        </div>
      </TabPane>
    ));

    return (
      <section className='emoji'>
        <Tabs tabPosition='bottom'>{tabPane}</Tabs>
      </section>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {
  useEmoji
}

export default connect(mapStateToProps, mapDispatchToProps)(Emoji);