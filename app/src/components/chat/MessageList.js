import React, { Component, ReactDOM } from 'react';
import { connect } from 'react-redux';

class MessageList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      messages: [],
      userName: this.props.cookies.get('name')
    }
  }

  componentDidMount() {
    this.scrollToBottom();
  }

  componentDidUpdate() {
    this.scrollToBottom();
  }

  scrollToBottom = () => {
    if (this.props.sendMessage) {
      this.endMessage.scrollIntoView({ behavior: "smooth" });
    }
  }

  render() {
    const messages = this.props.messages.map((message, index) => {
      let blockClassName = `message-block ${message.from}`;
      let messageClassName = `message ${message.from}`;
      let senderName = '';
      if (this.state.userName !== message.sender) {
        senderName = <span className="sender-name">{message.sender}:</span>
      }
      return (
        <div key={index} className={blockClassName}>
          <div className={messageClassName}>
            {senderName}
            {
              message.content.split('\n').map((line, i) => {
                return <span key={i}>{line}<br /></span>
              })
            }
          </div>
        </div>
      );
    });

    return (
      <section className='message-list'>
        {messages}
        <div ref={node => this.endMessage = node}></div>
      </section>
    );
  }
}

const mapStateToProps = state => ({ messages: state.MessageReducer.messages, sendMessage: state.MessageReducer.sendMessage });

const mapDispatchToProps = {}

export default connect(mapStateToProps, mapDispatchToProps)(MessageList);