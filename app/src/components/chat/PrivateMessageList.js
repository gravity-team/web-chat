import React, { Component } from 'react';
import { connect } from 'react-redux';
import PrivateMessage from './PrivateMessage';
import { toggle, addMessage, addTab } from '../../redux/action/PrivateMessageAction';
import { socketConfig } from '../../redux/constants/SocketConsstants';

class PrivateMessageList extends Component {
  constructor(props) {
    super(props);

    this.cookies = this.props.cookies;
    this.initSocket(this.props.socket);
  }

  initSocket = (socket) => {
    const that = this;
    socket.on(socketConfig.RECEIVE_PRIVATE_MESSAGE, (data) => {
      this.createIfNew(data);

      that.props.addMessage({ key: data.sender, body: { content: data.message, sender: data.senderName, from: 'other', type: 'text' }, sendMessage: false });
    });

    socket.on(socketConfig.RECEIVE_NOTIFY_SEND_FILE, (data) => {
      this.createIfNew(data);
    });
  }

  createIfNew = (data) => {
    let that = this;
    let block = document.getElementById(data.sender);
    if (block === null) {
      that.props.addTab({
        [data.sender]: {
          mask: data.sender,
          fullName: data.senderName,
          toggle: true,
          messages: [],
          sendMessage: false
        }
      });
    }
  }

  toggle = (event) => {
    this.props.toggle({ key: event.target.dataset.id, toggle: { toggle: true } });
  }

  render() {
    const privateMessages = Object.entries(this.props.privateMessages).map(([key, value], index) => (
      <div className='block' key={index} id={value.mask}>
        <div className='tab-block' data-id={key} onClick={this.toggle}>{value.fullName}</div>
        <PrivateMessage parent={this} hidden={!value.toggle} user={value} socket={this.props.socket} cookies={this.props.cookies} />
      </div>
    ));
    return (
      <section className='private-message-list flex'>
        {privateMessages}
      </section>
    );
  }
}

const mapStateToProps = state => ({ privateMessages: state.PrivateMessageReducer.privateMessages });

const mapDispatchToProps = {
  toggle,
  addMessage,
  addTab
}

export default connect(mapStateToProps, mapDispatchToProps)(PrivateMessageList);