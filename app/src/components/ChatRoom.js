import React, { Component } from 'react';
import MessageBox from './chat/MessageBox';
import MessageList from './chat/MessageList';
import OnlineUser from './chat/OnlineUser';
import PrivateMessageList from './chat/PrivateMessageList';
import { connect } from 'react-redux';
import { socketConfig } from '../redux/constants/SocketConsstants';
import { reset } from '../redux/action/MessageAction';
import { resetPrivate } from '../redux/action/PrivateMessageAction';

class ChatRoom extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      warning: '',
      password: '',
      oldPassword: ''
    }

    this.cookies = this.props.route.cookies;
    this.socket = this.props.route.socket;
    this.inputChange = this.inputChange.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    this.handleOk = this.handleOk.bind(this);
    this.popupModal = this.popupModal.bind(this);
    this.logout = this.logout.bind(this);
    this.initSocket(this.socket);
  }

  initSocket = (socket) => {
    let that = this;

    this.socket.on('ChangePasswordResult', (data) => {
      if (data.success) {
        that.setState({ visible: false, warning: '', oldPassword: '', password: '' });
      } else {
        that.setState({ warning: data.message });
      }
    });
  }

  inputChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  handleCancel = () => {
    this.setState({
      oldPassword: '',
      password: '',
      warning: '',
      visible: false
    });
  }

  handleOk = () => {
    let that = this;
    this.socket.emit(socketConfig.CHANGE_PASSWORD, {
      oldPassword: that.state.oldPassword,
      newPassword: that.state.password,
      username: that.cookies.get('username')
    });
  }

  popupModal = () => {
    this.setState({ visible: true });
  }

  logout = () => {
    this.cookies.remove('authToken');
    this.cookies.remove('name');
    this.cookies.remove('mask');
    this.socket.emit(socketConfig.SIGN_OUT);
    this.props.reset();
    this.props.resetPrivate();
    this.props.router.push('/');
  }

  render() {
    return (
      <section className="chat-room flex">
        <div className="login-form" hidden={!this.state.visible}>
          <div className="user-info flex flex-column">
            <input name="oldPassword" placeholder="Old Password" value={this.state.oldPassword} onChange={this.inputChange} />
            <input type="password" name="password" placeholder="Password" value={this.state.password} onChange={this.inputChange} />
          </div>
          <span hidden={!this.state.warning.length} className="message-warning">{this.state.warning}</span>
          <div className="button-group flex flex-wrap">
            <button className="login-button" onClick={this.handleOk}>Submit</button>
            <button className="go-signup-button" onClick={this.handleCancel}>Cancel</button>
          </div>
        </div>
        <div className="flex-20 flex flex-column left-menu">
          <OnlineUser socket={this.props.route.socket} />
          <div className='button-group flex flex-column'>
            <button className='change-password-button' onClick={this.popupModal}>Change Password</button>
            <button className='logout-button' onClick={this.logout}>Logout</button>
          </div>
        </div>
        <div className="flex-80 flex flex-column right-menu">
          <MessageList cookies={this.props.route.cookies} />
          <MessageBox socket={this.props.route.socket} cookies={this.props.route.cookies} />
          <PrivateMessageList socket={this.props.route.socket} cookies={this.props.route.cookies} />
        </div>
      </section>
    );
  }
}

const mapStateToProps = state => ({})

const mapDispatchToProps = {
  reset,
  resetPrivate
}

export default connect(mapStateToProps, mapDispatchToProps)(ChatRoom);