import React, { Component } from 'react';
import { connect } from 'react-redux';
import { socketConfig } from '../redux/constants/SocketConsstants';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      fullName: '',
      password: '',
      passwordConfirmation: '',
      wannaLogin: true,
      title: 'Login',
      passwordWarning: {
        value: false,
        message: ''
      },
      passwordConfirmationWarning: {
        value: false,
        message: ''
      },
      usernameWarning: {
        value: false,
        message: ''
      },
      warning: '',
      signupWarning: ''
    }

    this.cookies = props.route.cookies;
    this.socket = props.route.socket;
    this.inputChange = this.inputChange.bind(this);
    this.signup = this.signup.bind(this);
    this.signin = this.signin.bind(this);
    this.initSocket(this.socket, this.cookies);
  }

  initSocket = (socket, cookies) => {
    socket.on(socketConfig.SIGN_UP_RESULT, (data) => {
      if (data.success) {
        let date = new Date();
        date.setTime(date.getTime() + 7 * 24 * 60 * 60 * 1000);
        cookies.set('authToken', data.authToken, date);
        cookies.set('name', data.fullName, date);
        cookies.set('mask', data.mask, date);
        this.props.router.push('/chat-room');
      } else {
        this.setState({ signupWarning: data.message });
      }
    });
    socket.on(socketConfig.SIGN_IN_RESULT, (data) => {
      if (data.success) {
        let date = new Date();
        date.setTime(date.getTime() + 7 * 24 * 60 * 60 * 1000);
        cookies.set('authToken', data.authToken, date);
        cookies.set('name', data.fullName, date);
        cookies.set('mask', data.mask, date);
        this.props.router.push('/chat-room');
      } else {
        this.setState({ warning: data.message });
      }
    });
  }

  goLogin = () => {
    this.setState({ wannaLogin: true, title: 'Login', username: '', password: '', passwordConfirmation: '', fullName: '' });
  }

  goSignup = () => {
    this.setState({ wannaLogin: false, title: 'Signup', username: '', password: '' });
  }

  inputChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  signup = () => {
    if (this.state.password.length < 6) {
      this.setState({
        ...this.state,
        passwordWarning: {
          value: true,
          message: 'Password must be at least 6 characters.'
        }
      });
      return false;
    } else {
      this.setState({
        ...this.state,
        passwordWarning: {
          value: false,
          message: ''
        }
      });
    }
    if (this.state.password !== this.state.passwordConfirmation) {
      this.setState({
        passwordConfirmationWarning: {
          value: true,
          message: 'Password Confirmation must be matched with password.'
        }
      });
      return false;
    } else {
      this.setState({
        passwordConfirmationWarning: {
          value: false,
          message: ''
        }
      });
    }
    let date = new Date();
    date.setTime(date.getTime() + 7 * 24 * 60 * 60 * 1000);
    this.cookies.set('username', this.state.username, date);
    this.socket.emit(socketConfig.SIGN_UP, {
      username: this.state.username,
      password: this.state.password,
      passwordConfirmation: this.state.passwordConfirmation,
      fullName: this.state.fullName
    });
  }

  signin = () => {
    let date = new Date();
    date.setTime(date.getTime() + 7 * 24 * 60 * 60 * 1000);
    this.cookies.set('username', this.state.username, date);
    this.socket.emit(socketConfig.SIGN_IN, {
      username: this.state.username,
      password: this.state.password
    });
  }

  render() {
    return (
      <section className="app flex flex-column">
        <div className="title">{this.state.title}</div>
        <div className="login-form" hidden={!this.state.wannaLogin}>
          <div className="user-info flex flex-column">
            <input name="username" placeholder="Username" value={this.state.username} onChange={this.inputChange} />
            <input type="password" name="password" placeholder="Password" value={this.state.password} onChange={this.inputChange} />
          </div>
          <span hidden={!this.state.warning.length} className="message-warning">{this.state.warning}</span>
          <div className="button-group flex flex-wrap">
            <button className="login-button" onClick={this.signin}>Submit</button>
            <button className="go-signup-button" onClick={this.goSignup}>Signup</button>
          </div>
        </div>
        <div className="signup-form" hidden={this.state.wannaLogin}>
          <div className="user-info flex flex-column">
            <input name="fullName" placeholder="Full Name" value={this.state.fullName} onChange={this.inputChange} />
            <input name="username" placeholder="Username" value={this.state.username} onChange={this.inputChange} />
            <input type="password" name="password" placeholder="Password" value={this.state.password} className={this.state.passwordWarning.value ? 'warning' : ''} onChange={this.inputChange} />
            <span hidden={!this.state.passwordWarning.value} className="message-warning">{this.state.passwordWarning.message}</span>
            <input type="password" name="passwordConfirmation" placeholder="Password Confirmation" value={this.state.passwordConfirmation} className={this.state.passwordConfirmationWarning.value ? 'warning' : ''} onChange={this.inputChange} />
            <span hidden={!this.state.passwordConfirmationWarning.value} className="message-warning">{this.state.passwordConfirmationWarning.message}</span>
          </div>
          <span hidden={!this.state.signupWarning.length} className="message-warning">{this.state.signupWarning}</span>
          <div className="button-group flex flex-wrap">
            <button className="go-login-button" onClick={this.goLogin}>Login</button>
            <button className="signup-button" onClick={this.signup}>Submit</button>
          </div>
        </div>
      </section>
    );
  }
}

const mapStateToProps = state => ({})

const mapDispatchToProps = {}

export default connect(mapStateToProps, mapDispatchToProps)(App);