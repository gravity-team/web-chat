Object.defineProperty(String.prototype, 'humanize', {
  value() {
    return this.replace(/_/g, ' ').trim().replace(/\b[A-Z][a-z]+\b/g, (word) => {
      return word.toLowerCase()
    }).replace(/^[a-z]/g, (first) => {
      return first.toUpperCase()
    })
  }
});