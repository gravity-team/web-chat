import { secret } from './app/config/token_key';
import { status } from './app/config/http_status_code';
var express = require('express');
var jwt = require('jsonwebtoken');
exports.Initialize = function (apiRoutes) {
    apiRoutes.use(function (req, res, next) {

        console.log("DECODE");

        var token = req.body.token || req.param('token') || req.headers['x-access-token'];

        if (token) {
            jwt.verify(token, secret.key, function (err, decoded) {
                if (err) {
                    return res.status(status.Unauthorized).send({ success: false, message: 'Failed to authenticate token.' });
                } else {
                    // if everything is good, save to request for use in other routes
                    req.decoded = decoded;
                    next();
                }
            });
        } else {
            return res.status(status.Unauthorized).send({
                success: false,
                message: 'No token provided.'
            });

        }
    });
    console.log("===== token module is initialized.");
}