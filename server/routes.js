// port socket
const cors = require('cors')
const isDeveloping = process.env.NODE_ENV !== 'production';
const port = 9090;
const path = require('path');
import { routes } from './app/config/constants';
//key: id, value: socketId

const corsOptions = {
  origin: 'https://react-chat-uploader.herokuapp.com',
  optionsSuccessStatus: 200
}

exports.Initialize = (app) => {
  app.get('/admin', function (req, res) {
    res.sendfile(__dirname + '/AdminMain.html');
  });

  app.get('/*', cors(corsOptions), (req, res) => {
    res.sendFile(path.resolve(__dirname, '../app/build', 'index.html'));
  });
  app.get(routes.CHECK_CONNECT, (req, res) => {
    // success response
    res.send(
      {
        statusStr: true
      });
  });
  // app.get(routes.GET_USER_ONLINE, (req, res) => {
  //   res.status(200).json(DicUserOnline);
  // });
  console.log("===== routes module is initialized.");
}

