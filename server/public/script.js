$(document).ready(function () {
	var socket = io.connect();

	$('.chat_head').click(function () {
		$('.chat_body').slideToggle('slow');
	});
	$('.msg_head').click(function () {
		$('.msg_wrap').slideToggle('slow');
	});
	$('.close').click(function () {
		$('.msg_box').hide();
	});
	$('#signin').submit(function () {

		console.log('signin');

		let password = document.getElementById("password").value;

		let data = {
			username: $('#username').val(),
			password: document.getElementById("password").value
		};
		// console.log('username: ' + data.username);
		// console.log('password: ' + data.password);
		if (data.username == '' || data.password == '') {
			console.log('Invalid user input. Must provide a username, password.');
		} else {
			socket.emit('AdminSignin', data, () => { });
		}
		$('#password').val('');
		return false;
	});

	socket.on('signInResult', function (data) {
		if (data.success) {
			$('#nickWrap').hide();
			$('.chat_box').show();
			socket.emit('AdminSocket', 'Hi');
			socket.emit('adminRequestDictUserOnline', 'request');
			socket.emit('adminRequestShowLog', 'LOG');
		}
		else {

		}
		console.log('token:');
		console.log(data);
		//var authToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImFkbWluIiwiaWF0IjoxNTAzOTQzNDU3LCJleHAiOjE1MDQ1NDgyNTd9.gT3_7g1bZ78RxpnZn2xVcFraDetkA4U23xcQlXO5-Gw';
		//socket.emit('verifyAuthToken', { authToken: authToken });
	});

	socket.on('verifyAuthTokenResult', (data) => {
		console.log(data);
	});
	socket.on('adminReceiveDictUserOnline', function (data) {
		console.log('data.length: ' + Object.keys(data.DicUserOnline).length);
		console.log(Object.keys(data.DicUserOffline).length);
		//console.log(data.DicUserOnline[Object.keys(data.DicUserOnline)[0]].key);
		var html = '';
		for (i = 0; i < Object.keys(data.DicUserOnline).length; i++) {
			//console.log('data: ' + Object.keys(data.DicTest)[i]);
			html += '<div class="user" name="' + data.DicUserOnline[Object.keys(data.DicUserOnline)[i]].username + '"><span class="item_list_chat"> ' + data.DicUserOnline[Object.keys(data.DicUserOnline)[i]].username + '</span><span class="item_list_chat"> ' + data.DicUserOnline[Object.keys(data.DicUserOnline)[i]].ip + '</span><span class="item_list_chat"> ' + data.DicUserOnline[Object.keys(data.DicUserOnline)[i]].port + '</span></div>';
		}
		for (i = 0; i < Object.keys(data.DicUserOffline).length; i++) {
			//console.log('data: ' + Object.keys(data.DicTest)[i]);
			html += '<div class="userOffline" name="' + data.DicUserOffline[Object.keys(data.DicUserOffline)[i]] + '"><span class="item_list_chat"> ' + data.DicUserOffline[Object.keys(data.DicUserOffline)[i]] + '</span></div>';
		}
		//console.log(html);
		$('.chat_body').html(html);

	});
	socket.on('receiveListOnlineUser', function (data) {
		console.log('--receiveListOnlineUser');
		console.log(data);
	});

	socket.on('adminReceiveLog', (data) => {
		console.log(data.length);
		let dataSet = [];
		for (let i = 0; i < data.length; i++) {
			let dataItem = [];
			dataItem.push(data[i].RequestDate);
			dataItem.push(data[i].LogType);
			if (data[i].UserID == null) {
				data[i].UserID = 'anonymous';
			}
			dataItem.push(data[i].UserID);
			dataItem.push(data[i].Content);
			if (data[i].ResponseContent.message != null) {
				dataItem.push(data[i].ResponseContent.message);
			}
			else {
				dataItem.push(data[i].ResponseContent);
			}
			dataSet.push(dataItem);
		}
		$('#example').DataTable({
			data: dataSet,
			columns: [
				{ title: "RequestDate" },
				{ title: "LogType" },
				{ title: "UserID" },
				{ title: "Content" },
				{ title: "ResponseContent" }
			]
		});
	});
});
