exports.GetEmptyResponse = function()
{
    return {
        error:
        {
            id: 0,
            message: "SUCCESS"
        }
    };
}

exports.GetTimestamp = function()
{
    return Date.now();
}

exports.GenerateToken = function()
{
    var crypto = require('crypto');
    var current_date = (new Date()).valueOf().toString();
    var random = Math.random().toString();

    return crypto.createHash('sha1').update(current_date + random).digest('hex');
}

exports.GenerateID = function()
{
    return exports.GenerateToken() + exports.GetTimestamp();
}

exports.RandomIntInRange = function(min, max)
{
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
