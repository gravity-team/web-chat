exports.response = function(res, id, data)
{
    var errors = {
        1: 'TOKEN_IS_NOT_VALID',
        2: 'USER_NOT_FOUND',
        3: 'USER_ALREADY_EXISTS',
        4: 'APP_KEY_IS_NOT_VALID',
        5: 'EMAIL_ALREADY_EXISTS',
        6: 'INVALID_INPUT'
    };

    // send an error if we've not found a user with the requested data
    res.send(
    {
        error:
        {
            id: id,
            message: errors[id]
        }
    });
}
