global.__base = __dirname + '/';
global.DB = null;
require('babel-register');
require('babel-polyfill');

require('./prototypes');
require('import-export');
require('./app/config/constants');
const sha256 = require("js-sha256");
const path = require('path');
const express = require('express');
const mongo = require('mongodb');
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');
const shortId = require('shortid');
const dateTime = require('node-datetime');
const MongoClient = mongo.MongoClient;
const app = express();
const userModule = require('./user');
const routes = require('./routes');
const config = require('./config');
const token = require('./token');
const User = require('./app/schemes/User').User;
const Admin = require('./app/schemes/Admin').Admin;
const AppLog = require('./app/schemes/AppLog').AppLog;
const auth = require('./app/app_actions/auth').auth;
// mongo database host
const mongoDBHost = 'localhost';
// mongo database port
const mongoPort = 27017;
// mongo database name
const mongoDBName = 'WebChat';

const constants = require('./app/config/constants');
const signal = constants.signal;
const serverSendFile = constants.serverSendFile;
const tokenkey = require('./app/config/token_key');
const superSecret = tokenkey.secret;

const cookieParser = require('cookie-parser');
const methodOverride = require('method-override');
const session = require('express-session');

const isDeveloping = process.env.NODE_ENV !== 'production';
const secret = process.env.SECRET || 'CRBeL8o5JZsLOG4OFcjqWpr';
const port = isDeveloping ? 3000 : process.env.PORT;
const portSendFile = isDeveloping ? 3000 : process.env.PORT;
const addressSendFile = isDeveloping ? 'http://localhost:2020' : 'https://react-chat-uploader.herokuapp.com';
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(cookieParser())
app.use(methodOverride('X-HTTP-Method-Override'))
app.use(session({ secret, resave: false, saveUninitialized: true }))
app.use(express.static(path.join(__dirname, '../app/build')));
app.use(express.static(__dirname + '/public'));

let admin = {};
let AdminSK;
let ServerID;
let DicTest = {};
//key: Username, value: socket
let DicUser = {};

let DicUserOffline = {};
let DicUserOnline = {};
// key: mask, value: fullName
let DicMask = {};
// key: mask, value: Socket
let DicMaskSocket = {};
//key Username, value: Mask
let DicUsernameMask = {};
let ListUserOnline = [];
let mongoConnectionString = 'mongodb://' + mongoDBHost + ':' + mongoPort + '/' + mongoDBName;

if (!isDeveloping) {
  //   const ClientAsset = path.join(__dirname, '../app/build/asset-manifest.json');
  //   const ServerRendererPath = path.join(__dirname, './index.js');
  //   const ServerRenderer = require(ServerRendererPath).default;
  //   const Stats = require(ClientAsset);
  //   app.use(ServerRenderer(Stats));
  mongoConnectionString = 'mongodb://root:299199534@ds151433.mlab.com:51433/react-chat-app-demo';
}

// initialize mongo db connection
MongoClient.connect(mongoConnectionString, function (err, db) {
  if (err) {
    throw err;
  }
  // define global variable to use it everywhere
  global.DB = db;
});

// initialize player module
routes.Initialize(app);
config.Initialize(app);
// var apiRoutes = express.Router();
// token.Initialize(apiRoutes);
// app.use('/api', apiRoutes);
//userModule.Initialize(app);
// port for listening
const serverPort = 8080;
const server = require('http').createServer(app).listen(process.env.PORT || serverPort)
const io = require('socket.io').listen(server);





GetDictUserOffline = () =>{
  console.log('GetDictUserOffline');
  DB.collection('Users').find(
    {},
    { "_id": 0, "UserName": 1 })
    .toArray(function (err, docs) {
      if (err) {
        console.log(err);
      } else {
        if (docs != null) {
          for(let i = 0; i < docs.length; i++){
            DicUserOffline[docs[i].UserName] = docs[i].UserName;
          }
        }
      }
    });
}
let time = 2;
setInterval(() => {
  time--;
  if(time == 0){
    GetDictUserOffline();
    return;
  }
  //console.log(time);
}, 1000);


io.on('connection', (socket) => {
  let currentClient = {};
  currentClient.address = socket.request.connection._peername.address;
  currentClient.port = socket.request.connection._peername.port;
  // let clientIp = socket.request.connection._peername.address;
  // let clientPort = socket.request.connection._peername.port;
  // console.log(clientIp);
  // console.log(clientPort);

  /**
   * ServerSendFile
   */
  //luu SocketID server2
  socket.on(serverSendFile.SEND_SOCKET_ID, (data) => {
    ServerID = socket;
    //console.log(ServerID);
  });

  /**
   * admin
   */
  socket.on('AdminSignin', (data) => {
    console.log('**AdminSignin**');
    let response = {};
    try {
      if (!(data.username && data.password)) {
        response = {
          success: false,
          message: 'Invalid user input. Must provide a username, password.'
        };
      } else {
        let username = data.username;
        console.log(admin);
        if (username in admin) {
          response = {
            success: false,
            message: 'Account already in use.'
          };
          console.log('Account already in use.');
        } else {
          let passwordHash = sha256(data.password);
          let admin = new Admin();
          SignIn = () => {
            return new Promise((resolve, reject) => {
              admin.SignIn(username, passwordHash, function (success, errorCode) {
                if (success) {
                  admin.Push(function () {
                    console.log("id: " + admin.Document.FullName);
                    //socket.username = username;
                    admin[username] = socket;
                    response = {
                      success: true,
                      message: 'Sign in success.',
                      fullName: admin.Document.FullName
                    };

                    let Doc = { id: data.username };
                    let authToken = jwt.sign(Doc, superSecret.key, {
                      expiresIn: 604800 // expires in 7 days
                    });
                    response.authToken = authToken;
                    resolve(response);
                  });
                } else {
                  response = {
                    success: false,
                    message: 'username or password is invalid.'
                  };
                  resolve(response);
                }
              });
            });
          }
          SignIn()
            .then((result) => {
              socket.emit(signal.SIGN_IN_RESULT, result);
              if (result.success == true) {
                //SendListUserOnline();
                SendDicUserOnline();
              }
            })
            .catch((error) => {
              console.log(error);
            });
          //console.log(data);

        }
      }
    } catch (error) {
      console.log('error: ' + error);
    }

    //=>>add for demo 
    // let data1 = {
    //   username: 1,
    //   ip: 11,
    //   port: 111
    // };
    // let data2 = {
    //   username: 22222222,
    //   ip: 22,
    //   port: 222
    // };
    // DicUserOnline[data1.username] = data1;
    // DicUserOnline[data2.username] = data2;
    // SendDicUserOnline();
    //=>>add for demo 
  });

  socket.on(signal.ADMIN_REQUEST_SHOW_LOG, (data) => {
    console.log('**ADMIN_SHOW_LOG**');
    //console.log(DicUserOffline);
    appLog = new AppLog();
    DB.collection('AppLog').find(
      {},
      { "_id": 0 })
      .toArray(function (err, docs) {
        if (err) {
          console.log(err);
        } else {
          if (docs != null) {
            socket.emit(signal.ADMIN_RECEIVE_LOG, docs);
          }
        }
      });
  });

  GetAllUse = () =>{
    console.log('**GetAllUser**');

  }

  SendDicUserOnline = () => {
    console.log('**SendDicUserOnline**');
    console.log('DicUserOnline: ');
    console.log(DicUserOnline);
    console.log('AdminSK: ' + AdminSK);
    if (AdminSK != null) {
      console.log('**ADMIN_RECEIVE_DICT_USER_ONLINE**');
      console.log('DicUserOffline: ');
      console.log(DicUserOffline);
      socket.broadcast.to(AdminSK).emit(signal.ADMIN_RECEIVE_DICT_USER_ONLINE, {
        DicUserOnline: DicUserOnline,
        DicUserOffline: DicUserOffline
      });
    }
  }

  socket.on(signal.ADMIN_REQUEST_DICT_USER_ONLINE, (data) => {
    console.log('ADMIN_REQUEST_DICT_USER_ONLINE');
    console.log('DicUserOnline: ');
    console.log(DicUserOnline);
    console.log('DicUserOffline: ');
    console.log(DicUserOffline);
    socket.emit(signal.ADMIN_RECEIVE_DICT_USER_ONLINE, {
      DicUserOnline: DicUserOnline,
      DicUserOffline: DicUserOffline
    });
  })

  socket.on('AdminSocket', (data) => {
    AdminSK = socket.id;
    console.log('AdminSK');
    console.log(AdminSK);
  });

  //<=admin

  AddToListOnline = (id) => {
    console.log('**AddToListOnline**');
    return new Promise((resolve, reject) => {
      //=> add for test

      //=> add for test
      let user = new User();
      user.FindById(id, function () {
        if (user.IsInstantiated) {
          // fill a data
          let response = user.GetPublicData();
          //add to ListUserOnline
          if (response.UserName in DicUserOnline) {
            return;
          }

          DicUserOnline[response.UserName] = {
            username: response.UserName,
            ip: currentClient.address,
            port: currentClient.port
          };
          //delete ra danh sach offline
          delete DicUserOffline[response.UserName];
          DicUser[response.UserName] = socket.id;
          let mask;
          if (response.UserName in DicUsernameMask) {
            console.log('--Have user in dict!');
            mask = DicUsernameMask[response.UserName];
          } else {
            console.log('--Create new mask')
            mask = shortId.generate();
            DicUsernameMask[response.UserName] = mask;
          }
          socket.mask = mask;
          currentClient.mask = mask;
          currentClient.username = response.UserName;
          console.log('currentClient: ');
          console.log(currentClient);
          let valueOfMask = {
            mask: mask,
            fullName: response.FullName
          };

          ListUserOnline.push(valueOfMask);
          DicMask[mask] = valueOfMask;
          //add DicMaskSocket
          DicMaskSocket[mask] = socket;
          console.log(ListUserOnline);
        }
      });
    });
  }

  Decode = (authToken) => {
    return new Promise((resolve, reject) => {
      console.log('**Decode**');
      let id;
      if (authToken) {
        jwt.verify(authToken, superSecret.key, function (err, decoded) {
          if (err) {
            reject();
          } else {
            id = decoded.id;
            resolve(id);
          }
        });
      } else {
        id = null
        resolve(id);
      }
    });
  }

  AuthenticateToken = (authToken) => {
    console.log('**AuthenticateToken**');
    let response = {};
    return new Promise((resolve, reject) => {
      if (authToken) {
        jwt.verify(authToken, superSecret.key, function (err, decoded) {
          if (err) {
            response = {
              success: false,
              message: 'Authentication failed.'
            };
            resolve(response);
          } else {
            response = {
              success: true,
              message: 'Authentication successful.'
            };
            resolve(response);
          }
        });
      } else {
        response = {
          success: false,
          message: 'No token provided.'
        };
        resolve(response);
      }
    });
  }


  /**
   * data.authToken
   */

  socket.on(signal.VERIFY_AUTH_TOKEN, (data) => {
    console.log('**VERIFY_AUTH_TOKEN**');
    CheckAuthenticateToken(data.authToken);
  })

  /**
   * data.authToken
   */
  socket.on('UserReload', (data) => {
    // Decode(authToken)
    //   .then((result) => {
    //     return AddToListOnline(result);
    //   })
    //   .then((result2) => {

    //   })
    //   .catch((error) => {
    //     console.log(error);
    //   });
  })
  // check authenticate token
  CheckAuthenticateToken = (authToken) => {
    Decode(authToken)
      .then((result) => {
        return AddToListOnline(result);
      })
      .then((result2) => {

      })
      .catch((error) => {
        console.log(error);
      });
    AuthenticateToken(authToken)
      .then((result) => {
        socket.emit(signal.VERIFY_AUTH_TOKEN_RESULT, result);
        console.log(result);
      })
      .catch((error) => {
        console.log(error);
      });

  }
  // Handle the sending of messages
  socket.on('message', (data) => {
    console.log('SendPublicMessage: ');
    console.log(data);
    socket.broadcast.emit('receive', data);
  });

  /**
   * login
   * data.username
   * data.password
   */
  socket.on(signal.SIGN_IN, (data) => {
    console.log('**SIGN_IN**');
    let response = {};
    try {
      SignIn = () => {
        return new Promise((resolve, reject) => {
          if (!(data.username && data.password)) {
            response = {
              success: false,
              message: 'Invalid user input. Must provide a username, password.'
            };
            resolve(response);
          }
          else {
            let username = data.username;
            console.log('---username: ' + username);
            if (username in DicUserOnline) {
              response = {
                success: false,
                message: 'Account already in use.'
              };
              resolve(response);
            } else {
              let passwordHash = sha256(data.password);
              let user = new User();
              user.SignIn(username, passwordHash, function (success, errorCode) {
                console.log(`Result login: ${success}`);
                if (success) {
                  user.Push(function () {
                    //socket.username = username;
                    DicUser[username] = socket.id;
                    DicUserOnline[username] = {
                      username: username,
                      ip: currentClient.address,
                      port: currentClient.port
                    };
                    //xoa ra khoi danh sach offline
                    delete DicUserOffline[username];
                    //console.log('DicUserOnline[username]: ' + DicUserOnline[username]);
                    //add DicMask
                    let mask;
                    if (username in DicUsernameMask) {
                      console.log('--Have user in dict!');
                      mask = DicUsernameMask[username];
                    } else {
                      console.log('--Create new mask')
                      mask = shortId.generate()
                      DicUsernameMask[username] = mask;
                    }
                    socket.mask = mask;
                    currentClient.id = user.Document.id;
                    currentClient.mask = mask;
                    currentClient.username = username;
                    let valueOfMask = {
                      mask: mask,
                      fullName: user.Document.FullName
                    };
                    ListUserOnline.push(valueOfMask);
                    DicMask[mask] = valueOfMask;

                    //add DicMaskSocket
                    DicMaskSocket[mask] = socket;
                    response = {
                      success: true,
                      message: 'Sign in success.',
                      fullName: user.Document.FullName,
                      mask: mask
                    };
                    console.log(response);
                    //currentClient user.Document.id;
                    //add token
                    let Doc = { id: user.Document.id };
                    let authToken = jwt.sign(Doc, superSecret.key, {
                      expiresIn: 604800 // expires in 7 days
                    });
                    response.authToken = authToken;
                    //send new online user
                    console.log('currentClient');
                    console.log(currentClient);
                    NewOnlineUser(valueOfMask);
                    resolve(response);
                  });
                } else {
                  response = {
                    success: false,
                    message: 'username or password is invalid.'
                  };
                  console.log(response);
                  resolve(response);
                }
              });

            }
          }
        });
      }
      SignIn()
        .then((result) => {
          console.log('**SIGN_IN_RESULT**');
          console.log(result);
          socket.emit(signal.SIGN_IN_RESULT, result);
          //SendListUserOnline();
          //write log
          console.log('currentClient:');
          console.log(currentClient);
          let log = {
            logType: "Information log",
            content: "SIGN_IN",
            responseContent: result
          };
          WriteLog(log);
        })
        .catch((error) => {
          console.log(error);
          let log = {
            logType: "Warning log",
            content: "SIGN_IN",
            responseContent: error
          };
          WriteLog(log);
        });
    } catch (error) {
      console.error(error);
      let log = {
        logType: "Warning log",
        content: "SIGN_IN",
        responseContent: error
      };
      WriteLog(log);
    }
  });

  /**
   * 
   */
  NewOnlineUser = (data) => {
    console.log('**NewOnlineUser**');
    socket.emit(signal.NEW_ONLINE_USER, data);
  }

  // send list user online
  SendListUserOnline = () => {
    console.log('**SendListUserOnline**');
    console.log('ListUserOnline: ');
    console.log(ListUserOnline);
    // for (let i = 0; i < ListUserOnline.length; i++) {
    //   console.log('User: ' + ListUserOnline[i].mask);
    //   console.log('User: ' + ListUserOnline[i].fullName);
    //   console.log();
    // }
    Object.keys(DicUserOnline).forEach((username) => {
      // console.log('username: ' + username);
      // console.log('DicUser[username]: ' + DicUser[username]);
      io.to(DicUser[username]).emit(signal.RECEIVE_LIST_ONLINE_USER, ListUserOnline);
    }, this);
  }

  /**
   * Sign up
   * data.username
   * data.password
   * data.passwordConfirmation
   * data.fullName
   */
  socket.on(signal.SIGN_UP, (data) => {
    console.log('***SIGN_UP***');
    let response = {};
    try {
      if (!(data.username && data.password && data.fullName && data.passwordConfirmation)) {
        response = {
          success: false,
          message: 'Invalid user input. Must provide a username, password, passwordConfirmation, full name.'
        };
      } else {
        if (data.password != data.passwordConfirmation) {
          response = {
            success: false,
            message: 'Password confirmation must be matched with password.'
          };
        } else {
          let UserName = data.username;
          let Password = sha256(data.password);
          let FullName = data.fullName;
          // check if user already exist
          SignUp = () => {
            return new Promise((resolve, reject) => {
              DB.collection('Users').findOne({
                UserName: UserName
              },
                function (err, existedUser) {
                  if (err)
                    throw err;
                  // if there is a user with the same user name
                  if (existedUser) {
                    response = {
                      success: false,
                      message: 'username already exists.'
                    };
                    resolve(response);
                  }
                  else {
                    let user = new User();
                    // fill user object
                    user.SetUserName(UserName);
                    user.SetPassword(Password);
                    user.SetFullName(FullName);
                    //add to database
                    user.Push(function () {
                      //socket.username = UserName;
                      DicUser[UserName] = socket.id;
                      DicUserOnline[UserName] = {
                        username: UserName,
                        ip: currentClient.address,
                        port: currentClient.port
                      };
                      //console.log('Sign_Up: DicUserOnline[UserName]: ' + DicUserOnline[UserName]);
                      //add DicMask
                      let mask;
                      if (UserName in DicUsernameMask) {
                        console.log('--Have user in dict!');
                        mask = DicUsernameMask[UserName];
                      } else {
                        console.log('--Create new mask')
                        mask = shortId.generate();
                        DicUsernameMask[UserName] = mask;
                      }
                      socket.mask = mask;
                      let valueOfMask = {
                        mask: mask,
                        fullName: user.Document.FullName
                      };
                      ListUserOnline.push(valueOfMask);
                      DicMask[mask] = valueOfMask;
                      response = {
                        success: true,
                        message: 'Sign up success.',
                        fullName: user.Document.FullName,
                        mask: mask
                      };

                      //add DicMaskSocket
                      DicMaskSocket[mask] = socket;
                      currentClient.id = user.Document.id;
                      currentClient.mask = mask;
                      currentClient.username = UserName;
                      //currentClient = user.Document.id;
                      //add token
                      let Doc = { id: user.Document.id };
                      let authToken = jwt.sign(Doc, superSecret.key, {
                        expiresIn: 604800 // expires in 7 days
                      });
                      response.authToken = authToken;
                      resolve(response);
                    });
                  }
                });
            });
          }

          SignUp()
            .then((result) => {
              socket.emit(signal.SIGN_UP_RESULT, result);
              console.log('**SIGN_UP_RESULT**')
              console.log(result);
              if (result.success == true) {
                //SendListUserOnline();
                SendDicUserOnline();
                //write log
                let log = {
                  logType: "Information log",
                  content: "SIGN_UP",
                  responseContent: result
                };
                WriteLog(log);
              }
            })
            .catch((error) => {
              console.log(error);
              let log = {
                logType: "Warning log",
                content: "SIGN_UP",
                responseContent: error
              };
              WriteLog(log);
            });
        }
      }
    } catch (error) {
      console.error(error);
      let log = {
        logType: "Warning log",
        content: "SIGN_UP",
        responseContent: error
      };
      WriteLog(log);
    }
  });

  /**
   * data.id
   * data.username
   * data.oldPassword
   * data.newPassword
   */
  socket.on(signal.CHANGE_PASSWORD, (data) => {
    let response = {};
    let UserName;
    let OldPassword;
    let NewPassword;
    console.log('change password');
    ChangePass = () => {
      return new Promise((resolve, reject) => {
        //kiem tra nguoi dung dang nhap
        if (!(data.username && data.oldPassword && data.newPassword)) {
          response = {
            success: false,
            message: 'Invalid user input. Must provide a username, oldPassword, newPassword'
          };
          resolve(response);
        }
        else {
          if (data.username in DicUserOnline) {
            console.log(data);
            UserName = data.username;
            OldPassword = sha256(data.oldPassword);
            NewPassword = sha256(data.newPassword);
            // check old password
            let user = new User();
            user.SignIn(UserName, OldPassword, function (success, errorCode) {
              if (success) {
                //update password
                response = {
                  success: true,
                  message: 'Update password'
                };
                resolve(response);
              }
              else {
                response = {
                  success: false,
                  message: 'Wrong password!!'
                };
                resolve(response);
              }
            });
          }
          else {
            response = {
              success: false,
              message: 'Not sign in.'
            };
            resolve(response);
          }
        }
      });
    }
    ChangePass()
      .then((result) => {
        console.log('currentClient:');
        console.log(currentClient);
        if (result.success == false) {
          socket.emit('ChangePasswordResult', result);
          let log = {
            logType: "Information log",
            content: "CHANGE_PASSWORD",
            responseContent: result
          };
          WriteLog(log);
        }
        else {
          let user = new User();
          // fill user object
          user.SetUserName(UserName);
          user.SetPassword(NewPassword);
          //update to database
          user.UpdatePassword(function () {
            let rs = {
              success: true,
              message: 'Update password success.'
            };
            socket.emit('ChangePasswordResult', rs);
            //write log
            let log = {
              logType: "Information log",
              content: "CHANGE_PASSWORD",
              responseContent: rs
            };
            WriteLog(log);
          });
        }
      })
      .then((result2) => {

      })
      .catch((error) => {
        console.log(error);
        let log = {
          logType: "Warning log",
          content: "CHANGE_PASSWORD",
          responseContent: error
        };
        WriteLog(log);
      });
    //////////////////////

  });


  /**
   * Khi User mat ket noi, hoac tat trinh duyet
   */
  socket.on(signal.DISCONNECT, () => {
    console.log('***DISCONNECT***');
    console.log('Result: ' + RemoveFromUserOnline(currentClient));
  });
  /**
   * sign out
   */
  socket.on(signal.SIGN_OUT, () => {
    console.log('**SIGN_OUT**');
    let response = {
      success: true,
      message: 'Sign out success.'
    };
    console.log(response);
    try {
      socket.emit(signal.SIGN_OUT_RESULT, response);
      console.log('Result: ' + RemoveFromUserOnline(currentClient));
    } catch (error) {
      console.log(error);
    }

  });

  /**
   * Remove from DicUserOnline
   */
  RemoveFromUserOnline = (data) => {
    if (data.mask == null || data.username == null)
      return false;
    try {
      console.log('**RemoveFromUserOnline**');
      Object.keys(DicUserOnline).forEach(function (element) {
        console.log(element);
      }, this);
      delete DicUserOnline[data.username];
      delete DicMask[data.mask]
      DicUserOffline[data.username] = data.username;
      Object.keys(DicUserOnline).forEach(function (element) {
        console.log(element);
      }, this);
      for (let i = 0; i < ListUserOnline.length; i++) {
        if (ListUserOnline[i].mask == data.mask) {
          ListUserOnline.splice(i, 1);
        }
      }
      console.log('New list user online: ');
      console.log(ListUserOnline);
      console.log('**RECEIVE_LIST_ONLINE_USER**');
      socket.broadcast.emit(signal.RECEIVE_LIST_ONLINE_USER, ListUserOnline);
      //SendListUserOnline();
      SendDicUserOnline();
      return true;
    } catch (error) {
      console.log(error);
      return false;
    }

  }
  /**
   * 
   */
  socket.on(signal.REQUEST_LIST_ONLINE_USER, (data) => {
    console.log('**REQUEST_LIST_ONLINE_USER**');
    SendListUserOnline();
    SendDicUserOnline();
    // socket.emit(signal.RECEIVE_LIST_ONLINE_USER, ListUserOnline);
  });
  /**
   *  data.mask   //mask cua nguoi nhan
   *  data.senderName    //username cua user nhan tin nhan
   *  data.message //
   *
   */
  socket.on(signal.PRIVATE_MESSAGE, (data) => {
    try {
      console.log('**RECEIVE_PRIVATE_MESSAGE**');
      console.log('mask: ' + currentClient.mask);
      console.log('data.message: ' + data.message);
      console.log('senderName: ' + data.senderName);
      console.log('receiver: ' + data.mask);
      DicMaskSocket[data.mask].emit(signal.RECEIVE_PRIVATE_MESSAGE, {
        message: data.message,
        sender: currentClient.mask,
        senderName: data.senderName,
        receiver: data.mask
      });
    } catch (error) {
      console.log(error);
    }
  });

  socket.on('NOTIFY_POPUP_CHAT', (data) => {
    DicMaskSocket[data.mask].emit(signal.RECEIVE_NOTIFY_SEND_FILE, {
      sender: currentClient.mask,
      senderName: data.senderName,
      receiver: data.mask
    });
  });

  /**
   * send file
   * data.mask //mask cua nguoi nhan
   * data.senderName //username cua user nhan tin nhan
   */

  //Bước 1: ClientA sẽ báo Server truyền file cho ClientB
  socket.on(signal.REQUEST_SEND_FILE, (data) => {
    try {
      console.log('**REQUEST_SEND_FILE**');
      //Bước 2: Server chuyển thông tin báo ClientB là ClientA sắp chuyển file
      console.log('**RECEIVE_NOTIFY_SEND_FILE**');
      // let dataNotify = {
      //   sender: currentClient.mask,
      //   receive: data.mask
      // };
      //Bước 3: Server báo cho Server chịu trách nhiệm chuyển file
      console.log('**NOTIFY_SEND_FILE to server SendFile**');
      // ServerID.emit(serverSendFile.NOTIFY_SEND_FILE, dataNotify);
      /**
       * Bươc 5, 6: Server chuyển thông tin port mở cho ClientA và ClientB tương ứng
       */
      console.log('**RECEIVE_PORT_SEND_FILE**');
      console.log(data.mask);
      DicMaskSocket[data.mask].emit(signal.RECEIVE_PORT_SEND_FILE, {
        port: portSendFile,
        addressSendFile: addressSendFile
      });
      console.log(currentClient.mask);
      DicMaskSocket[currentClient.mask].emit(signal.RECEIVE_PORT_SEND_FILE, {
        port: portSendFile,
        addressSendFile: addressSendFile
      });
    } catch (error) {
      console.log(error);
    }
  });

  WriteLog = (data) => {
    let appLog = new AppLog();
    // fill user object
    let dt = dateTime.create();
    let formatted = dt.format('Y-m-d H:M:S');
    appLog.SetRequestDate(formatted);
    appLog.SetLogType(data.logType);
    appLog.SetUserID(currentClient.id);
    appLog.SetContent(data.content);
    appLog.SetResponseContent(data.responseContent);
    //add to database
    appLog.Push(function () {

    });
  }

});

// const data = {
//   UserName: "abcdefg",
//   Password: "7d1a54127b222502f5b79b5fb0803061152a44f92b37e23c6527baf665d4da9a"
// };
// const auth1 = new auth();
// const rs = auth1.SignIn(data);
// console.log("result: " + rs.success);
// let passwordHash = sha256('admin');
// console.log(passwordHash);