var auth = require(__base + 'app/app_actions/auth');
var userData = require(__base + 'app/data/user_data');

exports.Initialize = function(app)
{
    console.log("\n===== User module initialization is started");
    auth.Initialize(app);
    userData.Initialize(app);
    console.log("===== User module is initialized");
}

