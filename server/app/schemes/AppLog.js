var utils = require(__base + "utils");

exports.AppLog = function () {
    var In = this; // instance

    this.Document = {
        RequestDate: "",
        LogType: "",
        UserID: "",
        Content: "",
        ResponseContent: ""
    }

    this.IsInstantiated = false;

    // Setters

    this.SetRequestDate = (RequestDate) => {
        In.Document.RequestDate = RequestDate;
    }
    this.SetLogType = (LogType) => {
        In.Document.LogType = LogType;
    }
    this.SetUserID = (UserID) => {
        In.Document.UserID = UserID;
    }
    this.SetContent = (Content) => {
        In.Document.Content = Content;
    }
    this.SetResponseContent = (ResponseContent) => {
        In.Document.ResponseContent = ResponseContent;
    }

    // Getters
    this.GetPublicData = function () {
        return {
            RequestDate: In.Document.RequestDate,
            LogType: In.Document.LogType,
            UserID: In.Document.UserID,
            Content: In.Document.Content,
            ResponseContent: In.Document.ResponseContent
        };
    }

    this.GetListLog = function (callback) {
        console.log('DB: GetListLog');
        DB.collection('AppLog').find(
            {},
            { "RequestDate": 1, "LogType": 1, "UserID": 1, "Content": 1, "ResponseContent": 1 })
            .toArray(function (err, docs) {
                if (err) {
                    console.log(err);
                } else {
                    if (docs != null) {
                        callback(appLog);
                    }
                }
            });
        // DB.collection('AppLog').find(
        //     {

        //     }).toArray(function (err, appLog) {
        //         if (err)
        //             throw err;
        //         console.log(appLog);
        //         callback(appLog);
        //     });
    }

    this.Push = function (callback) {
        DB.collection('AppLog').insert(
            In.Document,
            function (err) {
                if (err)
                    throw err;
                callback();
            }
        );
    }

}
