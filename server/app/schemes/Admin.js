var utils = require(__base + "utils");

exports.Admin = function()
{
    var In = this; // instance

    this.Document = {
        UserName: "",
        Password: "",
        FullName: ""
    }

    this.IsInstantiated = false;

    // Setters
    this.SetUserName = function(UserName)
    {
        In.Document.UserName = UserName;
    }

    this.SetPassword = function(Password)
    {
        In.Document.Password = Password;
    }

    // Getters
    this.GetPublicData = function()
    {
        return {
            UserName: In.Document.UserName
        };
    }

    this.Push = function(callback)
    {
        console.log('---Push:' + In.Document.UserName);
        DB.collection('Admin').findOne(
        {
            UserName: In.Document.UserName
        }, function(err, existed)
        {
            if (err)
                throw err;
            if (existed)
            {
                DB.collection('Admin').update(
                    {
                        UserName: In.Document.UserName
                    },
                    In.Document,
                    function(err)
                    {
                        if (err)
                            throw err;

                        callback();
                    }
                );
            }
            else
            {
                DB.collection('Admin').insert(
                    In.Document,
                    function(err)
                    {
                        if (err)
                            throw err;

                        callback();
                    }
                );
            }
        });
    }

    this.Pull = function(callback)
    {
        DB.collection('Admin').findOne(
        {
            UserName: In.Document.UserName
        }, function(err, result)
        {
            if (err)
                throw err;

            if (result)
            {
                In.Document = result;

                In.IsInstantiated = true;

                callback(true, In);
            }
            else
            {
                callback(false, In);
            }
        });
    }

    this.SignIn = function(UserName, Password, callback)
    {
        console.log('----Admin SignIn');
        console.log('UserName: ' + UserName);
        console.log('Password: ' + Password);
        DB.collection('Admin').findOne(
        {
            UserName: UserName,
            Password: Password
        }, function(err, existedUser)
        {
            // throw an error if it is not null
            if (err)
                throw err;
            if (existedUser)
            {
                In.Document = existedUser;
                console.log('----existedUser: ' + existedUser);
                callback(true);
            }
            else
            {
                console.log('not existedUser');
                callback(false, 2);
            }
        });
    }


    
}
