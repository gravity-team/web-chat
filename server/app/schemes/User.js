var utils = require(__base + "utils");

exports.User = function () {
    var In = this; // instance

    this.Document = {
        id: utils.GenerateID(),
        UserName: "",
        Password: "",
        Email: "",
        FullName: "",
        fbID: ""
    }

    this.IsInstantiated = false;

    // Setters
    this.SetID = function (id) {
        In.Document.id = id;
    }

    this.SetUserName = function (UserName) {
        In.Document.UserName = UserName;
    }

    this.SetPassword = function (Password) {
        In.Document.Password = Password;
    }

    this.SetEmail = function (Email) {
        In.Document.Email = Email;
    }

    this.SetFullName = function (FullName) {
        In.Document.FullName = FullName;
    }

    this.SetFacebookID = function (facebookID) {
        In.Document.fbID = facebookID;
    }

    // Getters
    this.GetID = function () {
        return In.Document.id;
    }

    this.GetPublicData = function () {
        return {
            id: In.Document.id,
            UserName: In.Document.UserName,
            FullName: In.Document.FullName,
            Email: In.Document.Email
        };
    }

    this.Push = function (callback) {
        DB.collection('Users').findOne(
            {
                id: In.Document.id
            }, function (err, existed) {
                if (err)
                    throw err;

                if (existed) {
                    DB.collection('Users').update(
                        {
                            id: In.Document.id
                        },
                        In.Document,
                        function (err) {
                            if (err)
                                throw err;

                            callback();
                        }
                    );
                }
                else {
                    DB.collection('Users').insert(
                        In.Document,
                        function (err) {
                            if (err)
                                throw err;

                            callback();
                        }
                    );
                }
            });
    }

    this.Pull = function (callback) {
        DB.collection('Users').findOne(
            {
                id: In.Document.id
            }, function (err, result) {
                if (err)
                    throw err;

                if (result) {
                    In.Document = result;

                    In.IsInstantiated = true;

                    callback(true, In);
                }
                else {
                    callback(false, In);
                }
            });
    }

    this.SignIn = function (UserName, Password, callback) {
        DB.collection('Users').findOne(
            {
                UserName: UserName,
                Password: Password
            }, function (err, existedUser) {
                // throw an error if it is not null
                if (err)
                    throw err;
                if (existedUser) {
                    In.Document = existedUser;

                    callback(true);
                }
                else {
                    callback(false, 2);
                }
            });
    }

    this.SignInWithFacebook = function (facebookID, callback) {
        DB.collection('Users').findOne(
            {
                fbID: facebookID
            }, function (err, existedUser) {
                if (err)
                    throw err;

                if (existedUser) {
                    In.Document = existedUser;

                    callback(true);
                }
                else {
                    callback(false);
                }
            });
    }

    this.PullByToken = function (id, callback) {
        DB.collection('Users').findOne(
            {
                id: id
            }, function (err, user) {
                if (err)
                    throw err;
                if (user) {
                    In.Document = user;
                    In.IsInstantiated = true;
                }
                callback();
            });
    }

    this.FindById = (id, callback) => {
        DB.collection('Users').findOne(
            { id: id }, function (err, user) {
                if (err)
                    throw err;
                if (user) {
                    In.Document = user;
                    In.IsInstantiated = true;
                }
                callback();
            });
    }

    this.UpdatePassword = function (callback) {
        DB.collection('Users').findOne(
            {
                UserName: In.Document.UserName
            }, function (err, existed) {
                if (err)
                    throw err;
                if (existed) {
                    DB.collection('Users').update(
                        { UserName: In.Document.UserName },
                        { $set: { Password: In.Document.Password } },
                        function (err) {
                            if (err)
                                throw err;
                            callback();
                        }
                    );
                }
            });
    }
}
