export const auth = {
	SIGN_IN: '/Signin',
	SIGN_OUT: '',
	SIGN_UP: '/Signup',
	FACEBOOK_CONNECT: '/FacebookConnect'
}

export const user_data = {
	GET_USER_PROFILE: '/GetUserProfile' // + /api
}

export const routes = {
	GET_USER_ONLINE: '/GetUserOnline',
	CHECK_CONNECT: '/CheckConnection'
}

export const signal = {
	SIGN_IN: 'signin',
	SIGN_IN_RESULT: 'signInResult',
	SIGN_OUT: 'signout',
	SIGN_OUT_RESULT: 'signOutResult',
	SIGN_UP: 'signup',
	SIGN_UP_RESULT: 'signUpResult',
	CHANGE_PASSWORD: 'changePassword',
	DISCONNECT: 'disconnect',
	PRIVATE_MESSAGE: 'privateMessage',
	RECEIVE_PRIVATE_MESSAGE: 'receivePrivateMessage',
	AUTHENTICATION: 'authentication',
	VERIFY_AUTH_TOKEN: 'verifyAuthToken',
	VERIFY_AUTH_TOKEN_RESULT: 'verifyAuthTokenResult',
	REQUEST_LIST_ONLINE_USER: 'requestListOnlineUser',
	RECEIVE_LIST_ONLINE_USER: 'receiveListOnlineUser',
	NEW_ONLINE_USER: 'newOnlineUser',
	REQUEST_SEND_FILE: 'requestSendFile',
	RECEIVE_SEND_FILE: 'receiveSendFile',
	RECEIVE_NOTIFY_SEND_FILE: 'receiveNotifySendFile',
	RECEIVE_PORT_SEND_FILE: 'receivePortSendFile',
	ADMIN_REQUEST_SHOW_LOG: 'adminRequestShowLog',
	ADMIN_RECEIVE_LOG: 'adminReceiveLog',
	ADMIN_REQUEST_DICT_USER_ONLINE: 'adminRequestDictUserOnline',
	ADMIN_RECEIVE_DICT_USER_ONLINE: 'adminReceiveDictUserOnline'
}

export const serverSendFile = {
	SEND_SOCKET_ID: 'sendSocketID',
	NOTIFY_SEND_FILE : 'NotifySendFile',
	UPLOAD_FILE: 'uploadFile',
	UPLOAD_FILE_RESULT:'uploadFileResult',
	DOWNLOAD_FILE: 'downloadFile',
	DOWNLOAD_FILE_RESULT: 'downloadFileResult'
}