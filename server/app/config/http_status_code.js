export const status = {
	BadRequest: 400,
    Unauthorized: 401,
    NotFound: 404,
    OK: 200
}