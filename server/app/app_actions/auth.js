var utils = require(__base + 'utils');
var Security = require(__base + 'app/security/security').Security;
var errorResponse = require(__base + 'errors').response;
var User = require(__base + 'app/schemes/User').User;
var sha256 = require("js-sha256");
var jwt = require('jsonwebtoken');

import { auth } from '../config/constants';
import { secret } from '../config/token_key';
import { status } from '../config/http_status_code';

exports.Initialize = function (app) {
    // sign in method
    app.post(auth.SIGN_IN, function (req, res) {
            // get user data from a request body
            if (!(req.body.UserName && req.body.Password)) {
                return res.status(status.OK).send({
                    success: false,
                    message: 'Invalid user input. Must provide a UserName, Password'
                });
            }
            else {
                var requestedUserName = req.body.UserName;
                var requestedPasswordHash = sha256(req.body.Password);

                var user = new User();

                user.SignIn(requestedUserName, requestedPasswordHash, function (success, errorCode) {
                    console.log(success);
                    if (success) {
                        user.Push(function () {
                            // generate an empty response
                            var response = {};
                            console.log("id: " + user.Document.id);
                            var Doc = {
                                id: user.Document.id
                            };
                            var token = jwt.sign(Doc, secret.key, {
                                expiresIn: 604800 // expires in 7 days
                            });
                            // add token
                            response.token = token;
                            return res.status(status.OK).send({
                                success: true,
                                message: 'sign in success.',
                                data: response
                            });
                            
                        });
                    }
                    else {
                        // send an error if we've not found a user with the requested data
                        return res.status(status.OK).send({
                            success: false,
                            message: 'sign in failed.'
                        });
                    }
                });
            }

    });
    // sign up method
    app.post(auth.SIGN_UP, function (req, res) {
            if (!(req.body.UserName && req.body.Password && req.body.Email && req.body.FullName)) {
                return res.status(status.OK).send({
                    success: false,
                    message: 'Invalid user input. Must provide a UserName, Password, Email, FullName'
                });
            }
            else {
                // get user data from a request body
                console.log(req.body.UserName);
                var UserName = req.body.UserName;
                var Password = sha256(req.body.Password);
                var Email = req.body.Email;
                var FullName = req.body.FullName;
                // make a request to get a user with an username as we just got from the request to check if user already exist
                DB.collection('Users').findOne(
                    {
                        $or: [{ UserName: UserName }, { Email: Email }]
                    }
                    , function (err, existedUser) {
                        if (err)
                            throw err;

                        // if there is a user with the same user name
                        if (existedUser) {
                            // send an error
                            return res.status(status.OK).send({
                                success: false,
                                message: 'User already exists'
                            });
                        }
                        else {
                            // generate empty user object
                            var user = new User();

                            // fill user object fields
                            user.SetUserName(UserName);
                            user.SetPassword(Password);
                            user.SetEmail(Email);
                            user.SetFullName(FullName);

                            user.Push(function () {
                                // generate an empty response
                                var response = {
                                    success: true,
                                    message: 'Sign up success.'
                                };

                                // send a response
                                return res.status(status.OK).send(response);
                            });
                        }
                    });
            }

    });

    // facebook connect method
    app.post(auth.FACEBOOK_CONNECT, function (req, res) {
        // if request is secure
            var fbID = req.body.fbID;
            var UserName = req.body.UserName;
            var FullName = req.body.FullName;
            var user = new User();
            user.SignInWithFacebook(fbID, function (success) {
                // if a user already exists
                if (success) {

                    user.Push(function () {
                        // generate an empty response
                        var response = utils.GetEmptyResponse();
                        var Doc = {
                            id: user.Document.id
                        };
                        var token = jwt.sign(Doc, secret.key, {
                            expiresIn: 604800 // expires in 7 days
                        });
                        // add token
                        response.token = token;
                        // send a response
                        res.send(response);
                    });
                }
                else {
                    // create a new user if user with requested fbID does not exist 

                    // fill user object fields
                    user.SetUserID(fbID);
                    user.SetUserName(UserName);
                    user.SetFullName(FullName);
                    user.Push(function () {
                        // generate an empty response
                        var response = utils.GetEmptyResponse();

                        var Doc = {
                            id: user.id
                        };
                        var token = jwt.sign(Doc, secret.key, {
                            expiresIn: 604800 // expires in 7 days
                        });
                        response.token = token;
                        res.send(response);
                    });
                }
            });

    });

    console.log("===== Auth module is initialized.");
}
