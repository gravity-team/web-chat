var utils = require(__base + 'utils');
var User = require(__base + 'app/schemes/User').User;
var Security = require(__base + 'app/security/security').Security;
var errorResponse = require(__base + 'errors').response;

import { user_data } from '../config/constants';
import { status } from '../config/http_status_code';

exports.Initialize = function(app,apiRoutes)
{
    // returns an info about User profile
    apiRoutes.get(user_data.GET_USER_PROFILE, function (req, res)
    {
        var id = req.decoded.id;
        var user = new User();

        user.PullByToken(id, function () {
            if (user.IsInstantiated) {
 
                // fill a data
                var response = user.GetPublicData();

                // send a response
                return res.status(status.OK).send({
                    success: true,
                    message: 'get user profile success.',
                    data: response
                });
            }
            else {
                return res.status(status.OK).send({
                    success: false,
                    message: 'get user profile failed.'
                });
            }
        });
    });
    console.log("===== user_data module is initialized.");
}
