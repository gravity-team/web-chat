var express = require('express');

exports.Initialize = function(app)
{
	app.set('view engine', 'html');

	// Initialize the ejs template engine
	app.engine('html', require('ejs').renderFile);

	// Tell express where it can find the templates
	app.set('views',__base.replace('server','app') + 'public');

	// Make the files in the public folder available to the world
	app.use(express.static(__base.replace('server','app') + 'assets'));

	console.log("===== config module is initialized.");
}
