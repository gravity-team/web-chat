var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = mongoose.model('Users', new Schema({ 
    id: String, 
    UserName: String,
    Password: String,
    FullName: String,
    Email: String
}));